FROM php:7.3-apache

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# update and install apt-utils
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

# Install dev dependencies
RUN apt-get install -y $PHPIZE_DEPS \
    libtool \
    libxml2-dev \
    libmagickwand-dev \
    libmagickcore-dev

# Install production dependencies
RUN apt-get install -y \
    bash \
    g++ \
    gcc \
    git \
    imagemagick \
    libc-dev \
    libpng-dev \
    libpq-dev \
    make \
    mysql-client \
    nodejs \
    yarn \
    openssh-client \
    rsync \
    libzip-dev
    
# Clean up package cache
RUN apt-get clean -y

# Install PECL and PEAR extensions
RUN pecl install \
    imagick \
    xdebug

# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick \
    xdebug
RUN docker-php-ext-configure zip --with-libzip
RUN docker-php-ext-install \
    iconv \
    mbstring \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    pcntl \
    tokenizer \
    xml \
    gd \
    zip \
    bcmath

# Install composer
ENV COMPOSER_HOME /composer
ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

# Install PHP_CodeSniffer
RUN composer global require "squizlabs/php_codesniffer=*"

# Set working directory
WORKDIR /var/www/html
